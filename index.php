<?php

ini_set('display_errors', true);
error_reporting(E_ALL);

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $url = $_REQUEST['url'];

    if(isset($_SERVER['HTTP_TOKEN'])) {
        $token = $_SERVER['HTTP_TOKEN'];
    } else {
        $token = null;
    }


    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_REQUEST));

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'api-key: 2000',
            'token:' .$token
            )
    );

    $result = curl_exec($ch);

    echo $result;

    curl_close($ch);

}



