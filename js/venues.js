var cookie = JSON.parse(document.cookie);

if(cookie.length != 0) {
    document.getElementById("userAccount").innerHTML = "<p>"  + cookie.email + "</p>" +
        "<button type='button' class='btn btn-primary' onclick='logout()'>Logout</button>";


    function getVenues() {

        const data = {
            email: JSON.parse(document.cookie).email,
            url: 'http://test-venues-api.loc/clients/getVenues'
        };


        $.ajax({
            method: 'post',
            url: 'http://test-front.loc/index.php',
            headers: {
                token: JSON.parse(document.cookie).token
            },
            data: data,

            success: function (e) {

                let result = JSON.parse(e);
                var resultLength = result.data.venueList.length;

                if(result.success){
                    document.getElementById("myLocations").innerHTML = "";
                    for (let i = 0; i < resultLength; i++) {
                        document.getElementById("myLocations").innerHTML +=
                        "<p class='dropdown-item'>"  + result.data.venueList[i].name + "</p>"
                    }
                }
            }

        })
    }


    getVenues();


    function modalBody() {

        const data = {
            email: JSON.parse(document.cookie).email,
            url: 'http://test-venues-api.loc/clients/getVenues'
        };

        $.ajax({
            method: 'post',
            url: 'http://test-front.loc/index.php',
            headers: {
                token: JSON.parse(document.cookie).token
            },
            data: data,

            success: function (e) {

                let result = JSON.parse(e);
                var resultLength = result.data.venueList.length;

                if(result.success){
                    document.getElementById("modalBody").innerHTML = "";
                    for (let i = 0; i < resultLength; i++) {
                    document.getElementById("modalBody").innerHTML +=
                        "<div class='row'>" +
                            "<span class='col-md-7'>" +
                                "<p class='dropdown-item d-inline'>" + result.data.venueList[i].name + "</p>" +
                            "</span>" +
                            "<span class='col-md-5'>" +
                                "<button type='button' id='editVenueBtn' class='btn btn-outline-dark' style='margin-right: 15px'>" + "Edit" + "</button>" +
                                "<button type='button' id='deleteVenueBtn' class='btn btn-outline-danger'>" + "Remove" + "</button>" +
                            "</span>" +
                        "</div>" +
                        "<hr>"
                    }
                    document.getElementById("modalBody").innerHTML +=
                    "<div class='row'>" +
                        "<span class='col-md-7'>" +
                            "<input type='text' id='venueName' placeholder=' Add new venue'>" +
                        "</span>" +
                        "<span class='col-md-5'>" +
                            "<button onclick='addVenue()' type='submit' id='addVenueBtn' class='btn btn-outline-dark'>" + "Add" + "</button>" +
                        "</span>" +
                    "</div>"
                }
            }

        })
    }


    function addVenue() {
        let venueName = document.getElementById("venueName").value;

        const data = {
            email: JSON.parse(document.cookie).email,
            venueName: venueName,
            url: 'http://test-venues-api.loc/clients/addVenue'
        };


        $.ajax({
            method: 'post',
            url: 'http://test-front.loc/index.php',
            headers: {
                token: JSON.parse(document.cookie).token
            },
            data: data,
            success: function (e) {
                let result = JSON.parse(e);

                if(result.success){
                    alert(result.data.message);
                    getVenues();
                }
                else{
                    alert(result.error.message);
                }
            }
        })
    }



}

