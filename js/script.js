function logout() {
    document.cookie = null;
    window.close();
    window.open("login.html");
}


function register() {

    let email = document.getElementById("registerEmail").value;
    let password = document.getElementById("registerPassword").value;
    let repeatPassword = document.getElementById("registerRepeatPassword").value;

    const data = {
        email: email,
        password: password,
        repeatPassword: repeatPassword,
        url: 'http://test-accounts-api.loc/clients/register'
    };

    $.ajax({
        method: 'post',
        url: 'http://test-front.loc/index.php',
        data: data,
        success: function (e) {
            let result = JSON.parse(e);

            if(result.success){
                window.open("login.html");
            }
            else{
                alert(result.error.message);
            }
        }
    })

}


function login() {

    let email = document.getElementById("loginEmail").value;
    let password = document.getElementById("loginPassword").value;

    const data = {
        email: email,
        password: password,
        url: 'http://test-accounts-api.loc/clients/login'
    };

    $.ajax({
        method: 'post',
        url: 'http://test-front.loc/index.php',
        data: data,
        success: function (e) {
            let result = JSON.parse(e);

            if(result.success){
                let cookie = {
                    token: result.data.token,
                    email: email
                };
                document.cookie = JSON.stringify(cookie);
                window.open("venues.html");
            }
            else{
                alert(result.error.message);
            }
        }
    })

}



